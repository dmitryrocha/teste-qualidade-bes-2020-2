Narrativa:
Como um professor
desejo informar as duas notas de um aluno
de modo que possa saber se o mesmo foi aprovado

Cenário: Aluno aprovado com média superior à média de aprovação
Dado que um aluno está matriculado na disciplina
Quando informo a nota 8
E informo a nota 9
Então a situação do aluno é Aprovado

Cenário: Aluno aprovado com média igual à média de aprovação
Dado que um aluno está matriculado na disciplina
Quando informo a nota 9
E informo a nota 5
Então a situação do aluno é Aprovado

Cenário: Aluno reprovado com média inferior à média de aprovação
Dado que um aluno está matriculado na disciplina
Quando informo a nota 5
E informo a nota 3
Então a situação do aluno é Reprovado

Cenário: Aluno reprovado com média imediatamente inferior à média de aprovação
Dado que um aluno está matriculado na disciplina
Quando informo a nota 7.1
E informo a nota 6.8
Então a situação do aluno é Reprovado
